<h1> MORSE CODE </h2>

# 📗 Table of Contents

- [📖 About the Project](#about-project)
  - [🛠 Built With](#built-with)
    - [Tech Stack](#tech-stack)
    - [Key Features](#key-features)
- [💻 Getting Started](#getting-started)
  - [Setup](#setup)
  - [Prerequisites](#prerequisites)
- [👥 Authors](#authors)
- [🔭 Future Features](#future-features)
- [🤝 Contributing](#contributing)
- [⭐️ Show your support](#support)
- [🙏 Acknowledgements](#acknowledgements)
- [📝 License](#license)

# 📖 Decode Morse message <a name="about-project"></a>

**Decode Morse message** A simple morse code decoder. Using Ruby language.

## 🛠 Built With <a name="built-with"></a>

### Tech Stack <a name="tech-stack"></a>

- Languages: Ruby
- Frameworks: N/A
- Technologies Used: GIT, GITHUB, LINTERS

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## 💻 Getting Started <a name="getting-started"></a>

To get a local copy up and running, follow these steps.

### Prerequisites

In order to run this project you need:

- ruby installed and running

### Setup

- Clone this repo <https://github.com/Caren-Koroeny/enumarable-class.git>

  ```bash
  git clone https://github.com/Caren-Koroeny/enumarable-class.git
  ```

- Navigate to enumarate-massage folder/directory

  ```bash
  cd enumarate-massage
  ```

- On the command line, at the project's root, run `bundle install` to install app dependencies

- Next, run `ruby enumarate-massage.rb` which will run the app in the development mode.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## 👥 Authors <a name="authors"></a>

👤 **Christian Hakizimana**

- GitHub: [@githubhandle](https://github.com/hakichris)
- Twitter: [@twitterhandle](https://twitter.com/twitterhandle)
- LinkedIn: [LinkedIn](https://linkedin.com/in/hakichris)

👤 **Caren Siya**
- GitHub: [@carensiya ](https://github.com/Caren-Koroeny)
- Twitter: [@carensiya](https://twitter.com/home)
- LinkedIn: [@carensiya](www.linkedin.com/in/caren-siya-a89712180)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## 🔭 Future Features <a name="future-features"></a>

- [ ] Check more codes

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## 🤝 Contributing <a name="contributing"></a>

Contributions, issues, and feature requests are welcome!

Feel free to check the [issues page](../../issues/).

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## ⭐️ Show your support <a name="support"></a>

If you like this project, please give it a ⭐️!

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## 🙏 Acknowledgments <a name="acknowledgements"></a>

- Thanks to my learning and coding partners for all their support.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## 📝 License <a name="license"></a>

This project is [MIT](./LICENSE) licensed.

<p align="right">(<a href="#readme-top">back to top</a>)</p>